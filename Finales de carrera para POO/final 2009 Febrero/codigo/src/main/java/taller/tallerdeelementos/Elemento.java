
package taller.tallerdeelementos;

import java.util.HashMap;


public class Elemento {
    private static final String NOMBRE = "nombre";
    protected HashMap<String, Object> atributos;
    
    public Elemento(String nombreElemento){
        this.atributos = new HashMap<String, Object>();
        this.atributos.put(NOMBRE, nombreElemento);
    }
    
    
    public Object getAtributo(String nombreAtributo){
        return this.atributos.get(nombreAtributo);
    }
    
    public HashMap<String, Object> getAtributos(){
        //mapa de retorno
        HashMap<String, Object> retorno = new HashMap<String, Object>();
        
        //se utiliza este for para que se vayan copiando todos los elementos del mapa.
        for(HashMap.Entry<String,Object> entry : this.atributos.entrySet()){
            retorno.put(entry.getKey(),entry.getValue());
        }
        return retorno;
    }
    
    public void agregarAtributo(String nombreAtributo, Object valor){
        this.atributos.put(nombreAtributo, valor);
    }
    
    
    
}
