
package taller.tallerdeelementos;

public class TallerDeElementos {

    public static void main(String[] args) {
        /*Para hacer una mesa
            elemento             cantidad
        -----------------------------------
        material: metal       |   1
        material: madera      |   1
        herramienta: martillo |   1
        material: tornillo    |   10
        material: madera      |   5
        */
        Elemento metal = new Elemento("metal");
        metal.agregarAtributo("material", "metal");
        
        Elemento madera = new Elemento("madera");
        madera.agregarAtributo("material", "madera");
        
        Elemento tornillo = new Elemento("tornillo");
        tornillo.agregarAtributo("material", "tornillo");
        
        Taller tallercito = new Taller("taller");
        tallercito.agregarElemento(metal, 1);
        tallercito.agregarElemento(madera, 6);
        tallercito.agregarElemento(tornillo, 10);
        
        Plano pMartillo = new Plano("martillo");
        pMartillo.agregarElemento(metal, 1);
        pMartillo.agregarElemento(madera, 1);
        
        Plano pMesa = new Plano("mesa");
        pMesa.agregarElemento(madera,5);
        pMesa.agregarElemento(tornillo, 10);        
        
        
        /*
        Para crear los distintos elementos de stock se podria crear una funcion donde se muestren los distintos mensajes y condicionando con ifs.
        */
        /*
            Creando martillo para stock
        */
        System.out.println("Taller: " + tallercito.toString());
        System.out.println("Plano para martillo: " + pMartillo.toString());
        
        
        System.out.println("Puedo crear el martillo?: " + tallercito.esPosible(pMartillo));
        
        System.out.println("Creando martillo.....");
        tallercito.construir(pMartillo);
        
        System.out.println("Taller con martillo: " + tallercito.toString());
        
        /*
            Creando mesa para stock
        */
        System.out.println("Taller: " + tallercito.toString());
        System.out.println("Plano para mesa: " + pMesa.toString());
        
        
        System.out.println("Puedo crear la mesa?: " + tallercito.esPosible(pMesa));
        
        System.out.println("Creando mesa.....");
        tallercito.construir(pMesa);
        
        System.out.println("Taller con mesa y martillo: " + tallercito.toString());
        
    }
}
