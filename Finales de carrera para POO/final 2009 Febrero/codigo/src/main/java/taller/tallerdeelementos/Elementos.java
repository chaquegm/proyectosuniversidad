
package taller.tallerdeelementos;

import java.util.HashMap;


public class Elementos {
    
    private String nombre;
    protected HashMap<Elemento, Integer> elementos;
    
    public Elementos(String nombre){
        this.nombre = nombre;
        this.elementos = new HashMap<Elemento, Integer>();
    }
    
    public String getNombre(){
        return this.nombre;
    }
    
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    
    public HashMap<Elemento, Integer> getElementos(){
        HashMap<Elemento, Integer> retorno = new HashMap<Elemento, Integer>();
        
        for(HashMap.Entry<Elemento, Integer> entry : this.elementos.entrySet()){
            retorno.put(entry.getKey(),entry.getValue());
        }
        
        return retorno;
        
    }
    
    public void agregarElemento(Elemento e, Integer cantidad){
        if(!this.existeElemento(e)){
            this.elementos.put(e, cantidad);
        }else{
            //el elemento ya existe y le agregamos la cantidad de elementos que hay mas los que ya habian.
            this.elementos.replace(e, cantidad + this.getCantidad(e));
        }
    }
    
    
    
    public Integer getCantidad(Elemento e){
        for(HashMap.Entry<Elemento, Integer> entry : elementos.entrySet()){
            if(entry.getKey().equals(e)){
                return entry.getValue();
            }
        }
        return 0;
    }
    
    //quitamos la cantidad de elemntos de "e" y si ya no hay ninguno, lo quitamos del mapa.
    public void quitarElemento(Elemento e, Integer cantidad){
        if(this.existeElemento(e)){
            this.elementos.replace(e, this.getCantidad(e) - cantidad);
            if(this.getCantidad(e) <= 0){
                this.elementos.remove(e);
            }
        }
    }
    
    
    public boolean existeElemento(Elemento e){
        return elementos.containsKey(e);
    }
    
    
    @Override
    public String toString(){
        
        HashMap<Elemento, Integer> m = this.getElementos();
        String retorno = "\n";
        if(m != null){
            for(HashMap.Entry<Elemento, Integer> entry : m.entrySet()){
                retorno = retorno + " | Elemento: " + entry.getKey().getAtributo("nombre") + "   Cantidad:" + entry.getValue() + "\n";
            }
            return retorno;
        }else{
            return "No hay elementos";
        }
        
    }
    
    
    
    
}
