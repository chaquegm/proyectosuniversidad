

package taller.tallerdeelementos;

import java.util.Vector;

public class Plano extends Elementos{
    
    protected Vector<String> instrucciones;
    
    public Plano(String nombre) {
        super(nombre);
        this.instrucciones = new Vector<String>();
    }
    
    public Vector<String> getInstrucciones(){
        return (Vector<String>)instrucciones.clone();
    }
    
    public void agregarInstruccion(String instruccion){
        this.instrucciones.add(instruccion);
    }
    
}
