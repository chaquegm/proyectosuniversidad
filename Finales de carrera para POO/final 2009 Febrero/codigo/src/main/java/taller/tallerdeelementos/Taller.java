
package taller.tallerdeelementos;

import java.util.HashMap;
import java.util.Vector;

public class Taller extends Elementos{
    
    Vector<String> elementosStock;
    public Taller(String nombre) {
        super(nombre);
        elementosStock = new Vector<String>();
    }
    
    /*
    Si da true: va a ser posible crear el nuevo elemento del plano con las herramientas
    que dispongo en el taller.
    */
    public boolean esPosible(Plano plano){
        
        for(HashMap.Entry<Elemento, Integer> entry : plano.getElementos().entrySet()){
            if(!this.existeElemento(entry.getKey())){
                return false;
            }else{
                if(this.getCantidad(entry.getKey()) < entry.getValue()){
                    return false;
                }
            }
        }
        return true;
    }
    
    public void construir(Plano plano){
        if(this.esPosible(plano)){
            
            for(HashMap.Entry<Elemento, Integer> entry : plano.getElementos().entrySet()){
                this.quitarElemento(entry.getKey(), entry.getValue());
            }
            elementosStock.add(plano.getNombre());
        }
    }
    
    @Override
    public String toString(){
        String retorno = super.toString();
        retorno = retorno +"\n Lista de stock: \n" +elementosStock.toString();
        return retorno;
    }
    
    
    
}
