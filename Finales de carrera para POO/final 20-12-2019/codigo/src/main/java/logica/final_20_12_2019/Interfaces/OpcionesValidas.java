
package logica.final_20_12_2019.Interfaces;

import logica.final_20_12_2019.Pregunta;


public class OpcionesValidas implements Calculador{
    
    private Double puntajeFijo;
    
    OpcionesValidas(Double puntajeFijo){
        this.puntajeFijo = puntajeFijo;
    }
    
    public void setPuntajeFijo(Double nuevoPuntaje){
        this.puntajeFijo = nuevoPuntaje;
    }
    
    public Double getPuntajeFijo(){
        return this.puntajeFijo;
    }
    
    @Override
    public Double calcular(Pregunta p) {
        return p.getPuntajeFijo() + this.puntajeFijo * (p.opcionesRespuesta() / p.opcionesValidas());
    }
    
}
