
package logica.final_20_12_2019;


import java.util.Vector;
import logica.final_20_12_2019.Interfaces.Calculador;
import logica.final_20_12_2019.Interfaces.Validador;


public class PreguntaAbierta extends Pregunta {
    
    PreguntaAbierta(Calculador calcPuntaje, Seccion padre, Double puntajeFijo, Integer cteTiempo){
        super(calcPuntaje, padre, puntajeFijo, cteTiempo);
        this.temas = new Vector<String>();
        
    }
    
    PreguntaAbierta(Seccion padre, Double puntajeFijo, Integer cteTiempo){
        super(padre, puntajeFijo, cteTiempo);
        this.temas = new Vector<String>();
    }

    @Override
    public int opcionesRespuesta() {
        return (int)(Math.random()*10+1);
    }

    @Override
    public int opcionesValidas() {
        return (int)(Math.random()*10+1);
    }

    @Override
    public Examen restringirExamen(Validador validador) {
        if(this.examenValido(validador)){
            return new PreguntaAbierta(this.calculadorPuntaje, this.padre, this.puntajeFijo, this.cteTiempo);
        }
        return null;
    }

    
    
    


    
}
