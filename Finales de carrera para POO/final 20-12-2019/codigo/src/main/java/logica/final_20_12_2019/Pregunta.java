
package logica.final_20_12_2019;

import java.util.Vector;
import logica.final_20_12_2019.Interfaces.Calculador;
import logica.final_20_12_2019.Interfaces.Validador;


public abstract class Pregunta extends Examen{
    
    protected Seccion padre;
    protected Double puntajeFijo;
    protected Integer cteTiempo;
    protected Vector<String> temas;

    Pregunta(Calculador calcPuntaje, Seccion padre, Double puntajeFijo, Integer cteTiempo) {
        super(calcPuntaje);
        this.calculadorPuntaje = calcPuntaje;
        this.padre = padre;
        this.puntajeFijo = puntajeFijo;
        this.cteTiempo = cteTiempo;
        
        this.temas = new Vector<String>();
    }
    
    Pregunta(Seccion padre, Double puntajeFijo, Integer cteTiempo){
        super(padre.getCalculadorPuntaje());
        this.padre = padre;
        this.puntajeFijo = puntajeFijo;
        this.cteTiempo = cteTiempo;
    }
    
    
    
    
    @Override
    public Calculador getCalculadorPuntaje() {
        //en caso de que se modifique el calculador y se vuelva a null.
        if(calculadorPuntaje != null){
            return this.calculadorPuntaje;
        }
        return padre.getCalculadorPuntaje();
        
    }
    

    @Override
    public Double puntuacionTotal() {
        if(calculadorPuntaje != null){
            return calculadorPuntaje.calcular(this);
        }
        return this.puntajeFijo;
    }

    @Override
    public Integer tiempoEstimado() {
        return this.tiempoEstimado();
    }

    @Override
    public Integer cantidadDePreguntas() {
        return 1;
    }

    @Override
    public Vector<String> obtenerTemas() {
        return (Vector<String>)this.temas.clone();
    }

    
    @Override
    public boolean examenValido(Validador validador) {
        return validador.validar(this);
    }
    
    
    abstract public int opcionesRespuesta();
    abstract public int opcionesValidas();
    
    
    public Double getPuntajeFijo(){
        return this.puntajeFijo;
    }
    
    
    
    
    
    
    
    
}
