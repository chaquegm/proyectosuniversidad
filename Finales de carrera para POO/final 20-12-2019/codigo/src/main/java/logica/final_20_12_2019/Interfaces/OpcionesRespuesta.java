
package logica.final_20_12_2019.Interfaces;

import logica.final_20_12_2019.Pregunta;


public class OpcionesRespuesta implements Calculador{
    
    OpcionesRespuesta(){
        
    }
    @Override
    public Double calcular(Pregunta p) {
        return p.getPuntajeFijo() * p.opcionesRespuesta();
    }
    
}
