
package logica.final_20_12_2019;


import java.util.ArrayList;
import java.util.Vector;
import logica.final_20_12_2019.Interfaces.Calculador;
import logica.final_20_12_2019.Interfaces.Validador;

public class PreguntaMultiple extends Pregunta{
    
    protected ArrayList<Boolean> opcionesValidas;
    
    PreguntaMultiple(Calculador calcPuntaje, Seccion padre, Double puntajeFijo, Integer cteTiempo){
        super(calcPuntaje, padre, puntajeFijo, cteTiempo);
        this.opcionesValidas = new ArrayList<Boolean>();
    }
    
    PreguntaMultiple(Calculador calcPuntaje, Seccion padre, Double puntajeFijo, Integer cteTiempo, ArrayList<Boolean> opcionesValidas){
        super(calcPuntaje, padre, puntajeFijo, cteTiempo);
        this.opcionesValidas = opcionesValidas;
    }
    
    PreguntaMultiple(Seccion padre, Double puntajeFijo, Integer cteTiempo){
        super(padre, puntajeFijo, cteTiempo);
        this.opcionesValidas = new ArrayList<Boolean>();
    }

    @Override
    public int opcionesRespuesta() {
        return this.opcionesValidas.size();
    }

    @Override
    public int opcionesValidas() {
        int validos = 0;
        for(int i=0 ; i<this.opcionesValidas.size() ; i++){
            if(this.opcionesValidas.get(i)){
                validos ++;
            }
        }
        return validos;
    }

    @Override
    public Examen restringirExamen(Validador validador) {
        if(this.examenValido(validador)){
            return new PreguntaMultiple(this.calculadorPuntaje, this.padre, this.puntajeFijo, this.cteTiempo, this.opcionesValidas);
        }
        return null;
    }
    
    
    
 
    
    
    
    
}
