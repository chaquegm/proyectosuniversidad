
package logica.final_20_12_2019;

import java.util.Vector;
import logica.final_20_12_2019.Interfaces.Calculador;
import logica.final_20_12_2019.Interfaces.Validador;


public abstract class Examen {
    protected Calculador calculadorPuntaje;
    
    public Examen(Calculador c){
        this.calculadorPuntaje = c;
    }
    
    public Calculador getCalculadorPuntaje(){
        return this.calculadorPuntaje;
    }
    public void setCalculadorPuntaje(Calculador c){
        this.calculadorPuntaje = c;
    }
    
    abstract public Double puntuacionTotal();
    abstract public Integer tiempoEstimado();
    abstract public Integer cantidadDePreguntas();
    abstract public Vector<String> obtenerTemas();
    abstract public boolean examenValido(Validador validador);
    abstract public Examen restringirExamen(Validador validador);
    
    
}
