/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package logica.final_20_12_2019;

import java.util.Vector;
import logica.final_20_12_2019.Interfaces.Calculador;
import logica.final_20_12_2019.Interfaces.Validador;


public class Seccion extends Examen{

    protected Vector<Examen> subSecciones;
    
    Seccion(Calculador calcPuntaje){
        super(calcPuntaje);
        this.subSecciones = new Vector<Examen>();
    }
    
    

    @Override
    public Double puntuacionTotal() {
        Double sumPuntaje = 0.0;
        for(int i = 0 ; i < this.subSecciones.size() ; i++){
            sumPuntaje += subSecciones.get(i).puntuacionTotal();
        }
        return sumPuntaje;
    }

    @Override
    public Integer tiempoEstimado() {
        Integer sumTiempo = 0;
        for(int i = 0 ; i < this.subSecciones.size() ; i++){
            sumTiempo += subSecciones.get(i).tiempoEstimado();
        }
        return sumTiempo;
    }

    @Override
    public Integer cantidadDePreguntas() {
        Integer cantPreguntas = 0;
        for(int i = 0 ; i < this.subSecciones.size() ; i++){
            cantPreguntas += subSecciones.get(i).cantidadDePreguntas();
        }
        return cantPreguntas;
    }

    @Override
    public Vector<String> obtenerTemas() {
        Vector<String> temas = new Vector<String>();
        for(int i = 0 ; i < this.subSecciones.size() ; i++){
            Vector<String> temasAux= subSecciones.elementAt(i).obtenerTemas();
            for(int j = 0 ; j < temasAux.size() ; j++){
                if(!temas.contains(temasAux.elementAt(j))){
                    temas.add(temasAux.elementAt(j));
                }
            }
        }
        return temas;
    }

   

    @Override
    public Examen restringirExamen(Validador validador) {
        Seccion examenAux = new Seccion(this.calculadorPuntaje);
        
        for(int i = 0 ; i < this.subSecciones.size() ; i++){
           Examen e = this.subSecciones.elementAt(i).restringirExamen(validador);
           if(e != null){
               examenAux.agregarSeccion(e);
           }
        }
        
        return examenAux;
    }
    
    @Override
    public boolean examenValido(Validador validador) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
    public void agregarSeccion(Examen e){
        this.subSecciones.add(e);
    }
    
}
