
package logica.final_20_12_2019.Interfaces;

import logica.final_20_12_2019.Pregunta;


public interface Calculador {
    
    public Double calcular(Pregunta p);
}
