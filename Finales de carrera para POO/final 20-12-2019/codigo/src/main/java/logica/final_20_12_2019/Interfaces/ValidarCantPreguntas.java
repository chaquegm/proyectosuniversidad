
package logica.final_20_12_2019.Interfaces;

import logica.final_20_12_2019.Examen;


public class ValidarCantPreguntas implements Validador{
        
    private Integer cantPreguntas;
    
    ValidarCantPreguntas(Integer cantPreguntas){
        this.cantPreguntas = cantPreguntas;
    }
    
    public Integer cantPreguntas(){
        return this.cantPreguntas;
    }
    
    public void setCantPreguntas(Integer cantPreguntas){
        this.cantPreguntas = cantPreguntas;
    }
            
    
    @Override
    public boolean validar(Examen exam) {
        return exam.cantidadDePreguntas() >= cantPreguntas;
    }
    
}
