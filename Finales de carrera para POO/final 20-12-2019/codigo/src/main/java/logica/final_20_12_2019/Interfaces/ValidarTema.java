
package logica.final_20_12_2019.Interfaces;

import logica.final_20_12_2019.Examen;


public class ValidarTema implements Validador{
    
    private String tema;
    
    ValidarTema(String tema){
        this.tema = tema;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }
    
    @Override
    public boolean validar(Examen exam) {
        return exam.obtenerTemas().contains(this.tema);
    }
    
}
