
package logica.final_20_12_2019.Interfaces;

import logica.final_20_12_2019.Examen;

public class ValidarTiempo implements Validador {
    private Integer tiempoDado;
    
    ValidarTiempo(Integer tiempoDado){
        this.tiempoDado = tiempoDado;
    }

    public Integer getTiempoDado() {
        return tiempoDado;
    }

    public void setTiempoDado(Integer tiempo) {
        this.tiempoDado = tiempo;
    }
    
            
    @Override
    public boolean validar(Examen exam) {
        return exam.tiempoEstimado() <= this.tiempoDado;
    }
    
}
