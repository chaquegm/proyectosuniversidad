
package logica.final_20_12_2019.Interfaces;

import logica.final_20_12_2019.Examen;


public class ValidarPuntuacion implements Validador{
    private Double puntuacionDada;
    
    ValidarPuntuacion(Double puntuacionDada){
        this.puntuacionDada = puntuacionDada;
    }

    public Double getPuntuacionDada() {
        return puntuacionDada;
    }

    public void setPuntuacionDada(Double puntuacion) {
        this.puntuacionDada = puntuacion;
    }
    
    @Override
    public boolean validar(Examen exam) {
        return exam.puntuacionTotal() >= this.puntuacionDada;
    }
    
    
}
