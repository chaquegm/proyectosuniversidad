
package logica.final_20_12_2019.Interfaces;

import logica.final_20_12_2019.Examen;


public class ValidarNot implements Validador{
    private Validador validador;
    
    ValidarNot(Validador validador){
        this.validador = validador;
    }

    public Validador getValidador() {
        return validador;
    }

    public void setValidador(Validador validador) {
        this.validador = validador;
    }

    @Override
    public boolean validar(Examen exam) {
        return !validador.validar(exam);
    }
    
    
}
