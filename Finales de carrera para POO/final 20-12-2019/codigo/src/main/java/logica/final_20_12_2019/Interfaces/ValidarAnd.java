
package logica.final_20_12_2019.Interfaces;

import logica.final_20_12_2019.Examen;


public class ValidarAnd extends ValidarCompuesto {
    
    ValidarAnd(Validador v1, Validador v2){
        super(v1, v2);
        
    }
    
    @Override
    public boolean validar(Examen exam) {
        return (this.valid1.validar(exam) && this.valid2.validar(exam));
    }
    
}
