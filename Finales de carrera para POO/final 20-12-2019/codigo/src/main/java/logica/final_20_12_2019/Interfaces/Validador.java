
package logica.final_20_12_2019.Interfaces;

import logica.final_20_12_2019.Examen;


public interface Validador {
    
    public boolean validar(Examen exam);
}
