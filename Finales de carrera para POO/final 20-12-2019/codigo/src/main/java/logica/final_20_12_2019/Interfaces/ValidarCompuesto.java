/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package logica.final_20_12_2019.Interfaces;

import logica.final_20_12_2019.Examen;


public abstract class ValidarCompuesto implements Validador{
    
    protected Validador valid1;
    protected Validador valid2;
    
    ValidarCompuesto(Validador v1, Validador v2){
        this.valid1 = v1;
        this.valid2 = v2;
    }

    public Validador getValid1() {
        return valid1;
    }

    public Validador getValid2() {
        return valid2;
    }

    public void setValid1(Validador v1) {
        this.valid1 = v1;
    }

    public void setValid2(Validador v2) {
        this.valid2 = v2;
    }
    
    
    
}
